const id_refresh = "id_refresh" as string;
export const getKey = (): string | null => {
  return window.localStorage.getItem(id_refresh);
};
export const savKey = (key: string): void => {
  window.localStorage.setItem(id_refresh, key);
};
export const destroyKey = (): void => {
  window.localStorage.removeItem(id_refresh);
};

export default {getKey,savKey,destroyKey};