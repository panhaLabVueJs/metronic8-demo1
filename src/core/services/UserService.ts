const ID_USER = "id_user" as string;
export const getUser = (): string | null => {
  return window.localStorage.getItem(ID_USER);
};
export const saveUser = (user: string): void => {
  window.localStorage.setItem(ID_USER, user);
};
export const destroySession = (): void => {
  window.localStorage.removeItem(ID_USER);
};

export default { getUser, saveUser, destroySession };