const ID_ROLE = "id_role" as string;
export const getRole = (): string | null => {
  return window.localStorage.getItem(ID_ROLE);
};
export const saveRole = (role: string): void => {
  window.localStorage.setItem(ID_ROLE, role);
};
export const destroySession = (): void => {
  window.localStorage.removeItem(ID_ROLE);
};

export default {getRole,saveRole,destroySession};