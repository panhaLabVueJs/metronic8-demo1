export interface MenuItem {
  heading?: string;
  sectionTitle?: string;
  route?: string;
  isAdmin?: string;
  isUser?: string;
  isCustomer?: string;
  pages?: Array<MenuItem>;
  keenthemesIcon?: string;
  bootstrapIcon?: string;
  sub?: Array<MenuItem>;
}

const MainMenuConfig: Array<MenuItem> = [
  {
    isAdmin: "ROLE_ADMIN",
    isUser: "ROLE_USER",
    isCustomer: "ROLE_CUSTOMER",
    pages: [
      {
        heading: "dashboard",
        route: "/dashboard",
        keenthemesIcon: "graph-up",
        bootstrapIcon: "bi-app-indicator",
      },
      {
        heading: "order_management",
        route: "/pos/order",
        keenthemesIcon: "shop",
        bootstrapIcon: "bi-shop",
      },
      {
        heading: "order_history",
        route: "/pos/order-history/progress",
        keenthemesIcon: "cheque",
        bootstrapIcon: "bi-cheque",
      },
      {
        heading: "setting",
        route: "/builder",
        keenthemesIcon: "setting-2",
        bootstrapIcon: "setting-2",
      },
    ],
  },
  {
    isAdmin: "ROLE_ADMIN",
    // isUser: "ROLE_USER",
    heading: "Admin",
    route: "/apps",
    pages: [
      // {
      //   sectionTitle: "customers",
      //   route: "/customers",
      //   keenthemesIcon: "abstract-38",
      //   bootstrapIcon: "bi-printer",
      //   sub: [
      //     {
      //       heading: "gettingStarted",
      //       route: "/apps/customers/getting-started",
      //     },
      //     {
      //       heading: "customersListing",
      //       route: "/apps/customers/customers-listing",
      //     },
      //     {
      //       heading: "customerDetails",
      //       route: "/apps/customers/customer-details",
      //     },
      //   ],
      // },
      {
        sectionTitle: "inventory_management",
        route: "/inventory",
        keenthemesIcon: "basket-ok",
        bootstrapIcon: "basket-ok",
        sub: [
          {
            heading: "product",
            route: "/apps/products/products_list",
          },
          {
            heading: "product_low",
            route: "/apps/products/low",
          },
          {
            heading: "categories",
            route: "/apps/categories/categories_list",
          },
          // },
          // {
          //   heading: "Sub Categories",
          //   route: "/apps/sub_categories/sub_categories_list"
          // }
        ],
      },
      {
        sectionTitle: "mm_table",
        route: "/tables",
        keenthemesIcon: "element-7",
        bootstrapIcon: "bi-printer",
        sub: [
          {
            heading: "table",
            route: "/apps/tables/list",
          },
        ],
      },
      {
        sectionTitle: "user_management",
        route: "/users",
        keenthemesIcon: "profile-user",
        bootstrapIcon: "bi-user",
        sub: [
          {
            heading: "create_user",
            route: "/apps/user/create",
          },
          {
            heading: "user",
            route: "/apps/user/list",
          },
        ],
      },
      {
        sectionTitle: "report_management",
        route: "/report",
        keenthemesIcon: "chart-simple-2",
        bootstrapIcon: "chart-simple-2",
        sub: [
          {
            heading: "sale_daily",
            route: "/apps/report/daily",
          },
          {
            heading: "sale_list",
            route: "/apps/report/list",
          },{
            heading: "return_list",
            route: "/apps/report/return",
          },
        ],
      },
    ],
  },
];

export default MainMenuConfig;
