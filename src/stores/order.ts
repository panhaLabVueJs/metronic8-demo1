import { ref } from "vue";
import { defineStore } from "pinia";
import JwtService from "@/core/services/JwtService";
import axios from "axios";
import type {Product} from "@/stores/products";

import.meta.env.VITE_APP_API_URL;
export interface Dashboard{
  count_daily: number,
  count_monthly: number,
  sale_total: any
  products: Product
}
export interface Order {
  createAt: String,
  createBy: String,
  updateAt: [String, null],
  updateBy: [String, null],
  id: Number,
  tableSeat: Object,
  customer: Object,
  order_date: String,
  orderDescription: [String, null],
  total: Number,
  payment_method: [String, null],
  payment: String,
  payment_date: [String, null],
  status: String,
  orderItems: Array<Product>
}
export const useOrderStore = defineStore("order", () => {
  const token = `Bearer ${JwtService.getToken()}`;

  const index = ref(<Dashboard>{});
  async function dashboard() {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `api/app/order/dashboard`,
      headers: {
        accept: "*/*",
        Authorization: token,
      },
    };
    await axios
      .request(config)
      .then((response) => {
        index.value = response.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const listOrder = ref();

  async function getOrderList(status: any) {
    let data = JSON.stringify({
      id: 0,
      code: "string",
      description: "string",
      payment_method: "string",
      payment_status: "string",
      status: status,
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/order/list`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        listOrder.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const onDeleteOrder = async (id: any) => {
    // Logic to delete order
    let data = JSON.stringify({
      id: id,
      code: "string",
      description: "string",
      payment_method: "string",
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/order/delete-order`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const ViewOrderById = ref();

  async function getOrderById(Id: any) {
    let data = JSON.stringify({
      id: Id,
      code: "string",
      description: "string",
      payment_method: "string",
      payment_status: "string",
      status: "string",
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/order/view-order-id`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        ViewOrderById.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const getNewOrder = ref();

  async function createOrder(code: any, number_seat: any) {
    let data = JSON.stringify({
      id: 0,
      code: code,
      number_seat: number_seat,
      description: "string",
      payment_method: "string",
      payment_status: "string",
      status: "string",
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/order/create`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        // console.log(JSON.stringify(response.data));
        getNewOrder.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const addItemStatus = ref();

  async function addItem(order_id: any, product_id: any, qty: any, note: any) {
    let data = JSON.stringify({
      id: 0,
      order_id: order_id,
      product_id: product_id,
      qty: qty,
      note: note,
      return_status: "",
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/order-item/add`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        addItemStatus.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async function deleteItem(item_id: any) {
    let data = JSON.stringify({
      id: item_id,
      order_id: 0,
      product_id: 0,
      qty: 0,
      note: "string",
      return_status: "string",
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/order-item/delete`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async function updateItem(item_id: any, product_id: any, qty: any) {
    let data = JSON.stringify({
      id: item_id,
      order_id: 0,
      product_id: product_id,
      qty: qty,
      note: "",
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/order-item/update`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async function setNotification(id: any, table: any) {
    let data = JSON.stringify({
      id: id,
      table_id: table,
      title: "Kitchen",
      type: "K",
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/notifications/send-notification`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios
      .request(config)
      .then((response) => {
        // console.log(JSON.stringify(response.data));
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async function sentToKitchen(id: any) {
    let data = JSON.stringify({
      id: id,
      code: "string",
      number_seat: 0,
      description: "string",
      payment_method: "string",
      payment_status: "string",
      status: "string",
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/order/kitchen`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const paymentStatus = ref("");

  async function getPayment(order_id: any, type: any, description: any) {
    let data = JSON.stringify({
      id: order_id,
      code: "",
      description: description,
      payment_method: type,
      payment_status: "",
      status: "",
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/order/payment`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        paymentStatus.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }
  const messageReturn = ref("");
  async function returnItem(order_id:any, item: any, put: any, note: any){
    let data = JSON.stringify({
      "order_id": order_id,
      "product_id": 0,
      "order_item_id": item,
      "put_qty": put,
      "return_description": '',
      "status": "ACT",
      "note": note
    });

    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `api/app/order/add-return-item`,
      headers: {
        'accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': token
      },
      data : data
    };

    await axios.request(config)
        .then((response) => {
          messageReturn.value = response.data.message;
        })
        .catch((error) => {
          console.log(error);
        });

  }

  return {
    getOrderList,
    listOrder,
    onDeleteOrder,
    ViewOrderById,
    getOrderById,
    createOrder,
    getNewOrder,
    addItem,
    addItemStatus,
    deleteItem,
    updateItem,
    setNotification,
    sentToKitchen,
    getPayment,
    paymentStatus,
    dashboard,
    index,
    messageReturn,
    returnItem
  };
});
