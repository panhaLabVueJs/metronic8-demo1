import { ref } from "vue";
import { defineStore } from "pinia";
import JwtService from "@/core/services/JwtService";
import axios from "axios";

export interface Table {
  id: number;
  code: string,
  name: string,
  min_size: number;
  max_size: number;
  num_seat: number;
  status: string;
}

import.meta.env.VITE_APP_API_URL;
export const useTableStore = defineStore("table", () => {
  const token = `Bearer ${JwtService.getToken()}`;
  const imageUrl = ref();

  async function generateQrCode(text: string) {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `openapi/generate?text=` + text + `&width=300&height=300`,
      headers: {
        accept: "image/png"
      }
    };

    await axios
      .request(config)
      .then((response) => {
        // console.log(JSON.stringify(response.data));
        imageUrl.value = response.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const listTable = ref();

  async function getTable(page: any) {
    let data = JSON.stringify({
      "limit": 7,
      "page": page
    });

    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `api/app/table/list`,
      headers: {
        'accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': token
      },
      data : data
    };

    await axios.request(config)
        .then((response) => {
          listTable.value = response.data.data;
        })
        .catch((error) => {
          console.log(error);
        });

  }
  async function getTableList(){
    let data = JSON.stringify({
      "limit": 0,
      "page": 0,
      "id": 0,
      "name": "string",
      "name_kh": "string",
      "status": "string",
      "image": "string",
      "qty": 0,
      "coast": 0,
      "price": 0,
      "qty_alert": 0,
      "description": "string",
      "category_id": 0
    });
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `api/app/table/list`,
      headers: {
        'accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': token
      },
      data : data
    };

    await axios.request(config)
        .then((response) => {
          listTable.value = response.data.data;
        })
        .catch((error) => {
          console.log(error);
        });

  }

  const createTableStatus = ref();

  async function createTable(table: Table) {
    let data = JSON.stringify({
      "id": 0,
      "name": table.name,
      "min_size": table.min_size,
      "max_size": table.max_size,
      "num_seat": table.num_seat,
      "status": table.status
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/table/create`,
      headers: {

        "accept": "*/*",
        "Content-Type": "application/json",
        "Authorization": token
      },
      data: data
    };

    await axios.request(config)
      .then((response) => {
        // console.log(JSON.stringify(response.data));
        createTableStatus.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });

  }

  const getTableById = ref();

  async function editTable(id: any) {
    let data = JSON.stringify({
      "id": id,
      "min_size": 0,
      "max_size": 0,
      "num_seat": 0,
      "status": "string"
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/table/edit`,
      headers: {
        "accept": "*/*",
        "Content-Type": "application/json",
        "Authorization": token
      },
      data: data
    };
    await axios.request(config)
      .then((response) => {
        // console.log(JSON.stringify(response.data));
        getTableById.value = response.data.data;
        // console.log(editTable.value);
      })
      .catch((error) => {
        console.log(error);
      });

  }

  const updateTableStatus = ref();

  async function updateTable(value: Table) {
    let data = JSON.stringify({
      "id": value.id,
      "name": value.name,
      "min_size": value.min_size,
      "max_size": value.max_size,
      "num_seat": value.num_seat,
      "status": value.status
    });

    let config = {
      method: "put",
      maxBodyLength: Infinity,
      url: `api/app/table/update`,
      headers: {
        "accept": "*/*",
        "Content-Type": "application/json",
        "Authorization": token
      },
      data: data
    };
    await axios.request(config)
      .then((response) => {
        updateTableStatus.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });

  }

  const deleteStatus = ref();

  async function deleteTable(id: any) {
    let data = JSON.stringify({
      "id": id,
      "min_size": 0,
      "max_size": 0,
      "num_seat": 0,
      "status": "string"
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/table/delete`,
      headers: {
        "accept": "*/*",
        "Content-Type": "application/json",
        "Authorization": token
      },
      data: data
    };

    await axios.request(config)
      .then((response) => {
        deleteStatus.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });

  }
  const listOrder = ref();
  async function getListOrder() {
    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: 'http://localhost:2030/api/app/order/list',
      headers: {
        "accept": "*/*",
        "Content-Type": "application/json",
        "Authorization": token
      }
    };

    await axios.request(config)
      .then((response) => {
        // listOrder.value = response.data.data;
        console.log(JSON.stringify(response.data));
      })
      .catch((error) => {
        console.log(error);
      });

  }

  return {
    imageUrl,
    generateQrCode,
    getTable,
    listTable,
    createTable,
    createTableStatus,
    editTable,
    getTableById,
    updateTable,
    updateTableStatus,
    deleteTable,
    deleteStatus,
    getListOrder,
    listOrder,
    getTableList
  };
});
