import { ref } from "vue";
import { defineStore } from "pinia";
import JwtService from "@/core/services/JwtService";
import axios from "axios";

import.meta.env.VITE_APP_API_URL;
export const useReportStore = defineStore("report", () => {
  const token = `Bearer ${JwtService.getToken()}`;

  const listReport = ref();
  const amount = ref();
  const count = ref();

  async function getListReportByDate(startDate: any, endDate: any) {
    let data = JSON.stringify({
      id: 0,
      name: "",
      startDate: startDate,
      endDate: endDate,
      daily: "",
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/admin/report/start-end`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        listReport.value = response.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }
  async function getListReport(){

    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `api/app/admin/report/list`,
      headers: {
        'accept': '*/*',
        'Authorization': token
      }
    };

    await axios.request(config)
      .then((response) => {
        listReport.value = response.data;
        amount.value = response.data.total;
        count.value = response.data.count;
      })
      .catch((error) => {
        console.log(error);
      });

  }
  async function getReportDaily(){
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `api/app/admin/report/daily`,
      headers: {
        'accept': '*/*',
        'Authorization': token
      }
    };

    await axios.request(config)
        .then((response) => {
          listReport.value = response.data;
        })
        .catch((error) => {
          console.log(error);
        });

  }
  async function getMonthly(){
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `api/app/admin/report/monthly`,
      headers: {
        'accept': '*/*',
        'Authorization': token
      }
    };

    await axios.request(config)
        .then((response) => {
          amount.value = response.data.total;
          count.value = response.data.count;
          listReport.value = response.data;
        })
        .catch((error) => {
          console.log(error);
        });

  }
  const listReturn = ref([]);
  async function getListReturn(status: any){
    let data = JSON.stringify({
      "order_id": 0,
      "order_item_id": 0,
      "put_qty": 0,
      "return_description": "string",
      "status": status,
      "note": "string"
    });

    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `api/app/order/return-list`,
      headers: {
        'accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': token
      },
      data : data
    };

    await axios.request(config)
        .then((response) => {
          // console.log(JSON.stringify(response.data));
          listReturn.value = response.data.data;
        })
        .catch((error) => {
          console.log(error);
        });
  }

  return {
    getListReportByDate,
    listReport,
    getListReport,
    getReportDaily,
    getMonthly,
    amount,
    count,
    getListReturn,
    listReturn
  }
});
