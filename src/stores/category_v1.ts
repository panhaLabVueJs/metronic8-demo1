import { defineStore } from "pinia";
import JwtService from "@/core/services/JwtService";
import axios from "axios";
import { ref } from "vue";

export interface Category {
  id: any,
  image: any,
  name: string,
  name_kh: string,
  status: string,
  main_category_id: any,
  sort: any
}

import.meta.env.VITE_APP_API_URL;
export const useCategoryStore = defineStore("category", () => {
  const token = `Bearer ${JwtService.getToken()}`;
  const listCategories = ref();


  const listMainCategories = ref();

  async function getMainCategories() {

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/admin/main-category/list`,
      headers: {
        "accept": "*/*",
        "Authorization": token
      }
    };

    await axios.request(config)
      .then((response) => {
        listMainCategories.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });

  }

  async function getCategories() {
    let data = JSON.stringify({
      "limit": 0,
      "page": 0,
      "userId": 0,
      "status": "string",
      "id": 0,
      "categoryId": 0,
      "subCategoryId": 0,
      "name": "string",
      "sort": "string",
      "sortBy": "string",
      "add": 0
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `/api/app/category/list`,
      headers: {
        "accept": "*/*",
        "Content-Type": "application/json",
        "Authorization": token
      },
      data: data
    };

    await axios.request(config)
      .then((response) => {
        listCategories.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });


  }

  const imageUrl = ref();

  async function doUploadFile(file: any) {
    let data = new FormData();
    data.append("File", file);

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/public/v1/image/upload`,
      headers: {
        "accept": "*/*",
        "Authorization": token
      },
      data: data
    };

    await axios.request(config)
      .then((response) => {
        imageUrl.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });

  }

  const onCreateStatus = ref();

  async function onCreate(category: Category) {
    let data = JSON.stringify({
      "imageUrl": category.image,
      "name": category.name,
      "name_kh": category.name_kh,
      "status": category.status,
      "id": 0,
      "main_category_id": category.main_category_id,
      "sort": category.sort
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/category/create`,
      headers: {
        "accept": "*/*",
        "Content-Type": "application/json",
        "Authorization": token
      },
      data: data
    };

    await axios.request(config)
      .then((response) => {
        onCreateStatus.value = response.data.message;
      })
      .catch((error) => {
        console.log(error);
      });

  }

  const onDeleteStatus = ref();

  async function onDeleteCategory(id: any) {
    let data = JSON.stringify({
      "imageUrl": "string",
      "name": "string",
      "name_kh": "string",
      "status": "string",
      "id": id,
      "main_category_id": 0
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/category/delete`,
      headers: {
        "accept": "*/*",
        "Content-Type": "application/json",
        "Authorization": token
      },
      data: data
    };

    await axios.request(config)
      .then((response) => {
        onDeleteStatus.value = response.data.message;
      })
      .catch((error) => {
        console.log(error);
      });

  }

  const getEditCategory = ref();

  async function onEditCategory(id: any) {
    let data = JSON.stringify({
      "limit": 0,
      "page": 0,
      "userId": 0,
      "status": "",
      "id": 0,
      "categoryId": 0,
      "name": ""
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/category/${id}`,
      headers: {
        "accept": "*/*",
        "Content-Type": "application/json",
        "Authorization": token
      },
      data: data
    };

    await axios.request(config)
      .then((response) => {
        getEditCategory.value = response.data;
      })
      .catch((error) => {
        console.log(error);
      });

  }

  const updateStatus = ref();

  async function onUpdateCategory(category: Category) {

    let data = JSON.stringify({
      "imageUrl": category.image,
      "name": category.name,
      "name_kh": category.name_kh,
      "status": category.status,
      "id": category.id,
      "main_category_id": category.main_category_id,
      "sort": category.sort
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/category/update`,
      headers: {
        "accept": "*/*",
        "Content-Type": "application/json",
        "Authorization": token
      },
      data: data
    };

    await axios.request(config)
      .then((response) => {
        updateStatus.value = response.data.message;
      })
      .catch((error) => {
        console.log(error);
      });

  }

  return {
    getCategories,
    listCategories,
    doUploadFile,
    imageUrl,
    onDeleteStatus,
    onDeleteCategory,
    onEditCategory,
    getEditCategory,
    onUpdateCategory,
    updateStatus,
    onCreate,
    getMainCategories,
    listMainCategories,
    onCreateStatus
  };
});