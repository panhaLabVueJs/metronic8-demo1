import { ref } from "vue";
import { defineStore } from "pinia";
import JwtService from "@/core/services/JwtService";

import PayloadService from "@/core/services/PayloadService";
import axios from "axios";
import UserService from "@/core/services/UserService";

import.meta.env.VITE_APP_API_URL;

export interface userDetail {
  id: any;
  profile: string;
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  phoneNumber: string;
  password: string;
  confirmPassword: string;
  group: any;
  status: string;
  role: string;
}

export interface DataPassword {
  id: any;
  phone: string;
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
}

export const useUserStore = defineStore("user", () => {
  const userRole = ref({});
  const token = `Bearer ${JwtService.getToken()}`;
  const errorToken = ref({});

  function setErrorToken(error: any) {
    errorToken.value = { ...error };
  }

  async function getMe() {
    let data = JSON.stringify({
      refreshToken: PayloadService.getKey(),
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `oauth/get-me`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };
    await axios
      .request(config)
      .then((response) => {
        UserService.saveUser(JSON.stringify(response.data.user));
      })
      .catch((error) => {
        setErrorToken(error);
      });
  }

  const createUserStatus = ref("");

  async function createUser(user: userDetail, role: any) {
    let data = JSON.stringify({
      id: 0,
      firstName: user.firstName,
      lastName: user.lastName,
      username: user.username,
      email: user.email,
      password: user.password,
      confirmPassword: user.confirmPassword,
      phoneNumber: user.phoneNumber,
      profile: user.profile,
      role: role,
      group: user.group,
      status: "ACT",
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/admin/user/create`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        createUserStatus.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const updateStatus = ref();

  async function updateUser(user: userDetail, role: any) {
    let data = JSON.stringify({
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      username: user.username,
      email: user.email,
      phoneNumber: user.phoneNumber,
      profile: user.profile,
      role: user.group,
      group: user.group,
      status: "ACT",
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/admin/user/update`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        updateStatus.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const listUser = ref();

  async function getListUser() {
    let data = JSON.stringify({
      id: 0,
      userAccountId: "string",
      shopId: "string",
      deviceId: "string",
      status: "ACT",
      outletId: 0,
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/admin/user/list`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        listUser.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const userInfo = ref(<userDetail>{});

  async function getUserById(id: any) {
    let data = JSON.stringify({
      limit: 0,
      page: 0,
      userId: 0,
      status: "string",
      id: 0,
      categoryId: 0,
      subCategoryId: 0,
      name: "string",
      sort: "string",
      sortBy: "string",
      add: 0,
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/admin/user/` + id,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        // console.log(JSON.stringify(response.data.data));
        userInfo.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const updatePasswordStatus = ref<any>();

  async function updatePassword(
    id: any,
    old: any,
    n_pass: any,
    conf_pass: any
  ) {
    let data = JSON.stringify({
      deviceId: "string",
      phoneNumber: "string",
      oldPassword: old,
      password: n_pass,
      confirmPassword: conf_pass,
      otp: "string",
      id: id,
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/oauth/forgot/password/finish`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        // console.log(JSON.stringify(response.data));
        updatePasswordStatus.value = response.data.message;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const deleteStatus = ref("");

  async function deleteUser(id: any) {
    let data = JSON.stringify({
      id: id,
      firstName: "string",
      lastName: "string",
      username: "string",
      email: "string",
      phoneNumber: "string",
      password: "string",
      confirmPassword: "string",
      profile: "string",
      role: "string",
      group: "string",
      status: "string",
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/admin/user/delete`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        // console.log(JSON.stringify(response.data));
        deleteStatus.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  return {
    getMe,
    userRole,
    errorToken,
    createUserStatus,
    createUser,
    listUser,
    getListUser,
    userInfo,
    getUserById,
    updatePassword,
    updateStatus,
    updateUser,
    updatePasswordStatus,
    deleteStatus,
    deleteUser,
  };
});
