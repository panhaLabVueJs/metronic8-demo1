import { ref } from "vue";
import { defineStore } from "pinia";
import ApiService from "@/core/services/ApiService";
import JwtService from "@/core/services/JwtService";
import RoleService from "@/core/services/RoleService";
import PayloadService from "@/core/services/PayloadService";
import axios from "axios";
import UserService from "@/core/services/UserService";

import.meta.env.VITE_APP_API_URL;

export interface User {
  username: string;
  email: string;
  password: string;
  accessToken: string;
  refreshToken: string;
}

export const useAuthStore = defineStore("auth", () => {
  const errors = ref({});
  const user = ref<User>({} as User);
  const isAuthenticated = ref(!!JwtService.getToken());
  const token = `Bearer ${JwtService.getToken()}`;

  function setRole(role: any) {
    RoleService.saveRole(role);
  }

  function setAuth(token: any, id_refresh: any) {
    isAuthenticated.value = true;
    errors.value = {};
    JwtService.saveToken(token);
    PayloadService.savKey(id_refresh);
  }

  function setError(error: any) {
    errors.value = { ...error };
  }

  function purgeAuth() {
    isAuthenticated.value = false;
    user.value = {} as User;
    errors.value = [];
    JwtService.destroyToken();
    PayloadService.destroyKey();
    UserService.destroySession();
  }

  async function login(user: User) {
    const data = JSON.stringify({
      username: user.username,
      password: user.password
    });

    const config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/oauth/token`,
      headers: {
        "Content-Type": "application/json"
      },
      data: data
    };
    await axios
      .request(config)
      .then((response) => {
        setAuth(response.data.accessToken, response.data.refreshToken);
        setRole(response.data.user.group.name);
        UserService.saveUser(JSON.stringify(response.data.user));
      })
      .catch((error) => {
        setError(error.code);
      });
  }

  function logout() {
    purgeAuth();
    RoleService.destroySession();
  }

  function forgotPassword(email: string) {
    return ApiService.post("forgot_password", email)
      .then(() => {
        setError({});
      })
      .catch(({ response }) => {
        setError(response.data.errors);
      });
  }

  function verifyAuth() {
    if (!JwtService.getToken()) {
      purgeAuth();
    }
  }

  async function refreshToken() {

    let data = JSON.stringify({
      "refreshToken": PayloadService.getKey()
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/oauth/refresh`,
      headers: {
        "accept": "*/*",
        "Content-Type": "application/json",
        "Authorization": token
      },
      data: data
    };

    await axios.request(config)
      .then((response) => {
        purgeAuth();
        setAuth(response.data.accessToken, response.data.refreshToken);
        RoleService.destroySession();
        setRole(response.data.user.group.name);
        UserService.saveUser(JSON.stringify(response.data.user));
      })
      .catch((error) => {
        console.log(error);
      });

  }

  return {
    errors,
    user,
    isAuthenticated,
    login,
    logout,
    forgotPassword,
    verifyAuth,
    refreshToken
  };
});
