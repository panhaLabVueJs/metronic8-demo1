import { ref } from "vue";
import { defineStore } from "pinia";
import JwtService from "@/core/services/JwtService";
import axios from "axios";

export interface MainCategory {
  "id": number,
  "name": string,
  "name_kh": string,
  "imageUrl": null,
  "status": string,
}

export interface Category {
  "id": number,
  "name": string,
  "name_kh": string,
  "imageUrl": null,
  "status": string,
  mainCategory: MainCategory
}

export interface Product {
  id: 0;
  code: string;
  name: string;
  name_kh: string;
  status: string;
  image: string;
  qty: number;
  coast: any;
  price: any;
  qty_alert: 3;
  description: string;
  category: Category
}

import.meta.env.VITE_APP_API_URL;
export const useProductStore = defineStore("product", () => {
  const token = `Bearer ${JwtService.getToken()}`;
  const listProducts = ref();

  async function getProducts() {
    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/admin/product/list`,
      headers: {
        accept: "*/*",
        Authorization: token
      }
    };

    await axios
      .request(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        listProducts.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async function getProductsLow() {
    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/admin/product/low`,
      headers: {
        accept: "*/*",
        Authorization: token
      }
    };

    await axios
      .request(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
        listProducts.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const createProductStatus = ref();

  async function createProduct(product: Product) {
    let data = JSON.stringify({
      id: 0,
      name: product.name,
      name_kh: product.name_kh,
      status: product.status,
      image: product.image,
      qty: product.qty,
      coast: product.coast,
      price: product.price,
      qty_alert: 3,
      description: product.description,
      category_id: product.category.id
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/admin/product/create`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token
      },
      data: data
    };

    await axios
      .request(config)
      .then((response) => {
        createProductStatus.value = response.data.message;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const updateProductStatus = ref();

  async function updateProduct(product: Product) {
    let data = JSON.stringify({
      id: product.id,
      name: product.name,
      name_kh: product.name_kh,
      status: product.status,
      image: product.image,
      qty: product.qty,
      coast: product.coast,
      price: product.price,
      qty_alert: 3,
      description: product.description,
      category_id: product.category.id
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/admin/product/update`,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
        Authorization: token
      },
      data: data
    };

    await axios
      .request(config)
      .then((response) => {
        updateProductStatus.value = response.data.message;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const productById = ref();

  async function getProductById(id: any) {
    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/admin/product/` + id,
      headers: {
        accept: "*/*",
        Authorization: token
      }
    };

    await axios
      .request(config)
      .then((response) => {
        productById.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }
const statusDelete = ref();
  async function onDelete(id: any) {
    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/admin/product/delete/` + id,
      headers: {
        "accept": "*/*",
        "Authorization": token
      }
    };

    await axios.request(config)
      .then((response) => {
        statusDelete.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });

  }

  async function searchByName(name: any) {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `api/app/admin/product/search?name=` + name,
      headers: {
        "accept": "*/*",
        "Authorization": token
      }
    };

    axios.request(config)
      .then((response) => {
        listProducts.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });

  }

  const status = ref("");

  async function addStockQty(pro_id: any, add_qty: any) {
    let data = JSON.stringify({
      "limit": 0,
      "page": 0,
      "id": pro_id,
      "name": "string",
      "name_kh": "string",
      "status": "string",
      "image": "string",
      "qty": add_qty,
      "coast": 0,
      "price": 0,
      "qty_alert": 0
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `api/app/admin/product/addStock`,
      headers: {
        "accept": "*/*",
        "Content-Type": "application/json",
        "Authorization": token
      },
      data: data
    };

    await axios.request(config)
      .then((response) => {
        status.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });

  }

  return {
    listProducts,
    getProductsLow,
    getProducts,
    createProductStatus,
    createProduct,
    productById,
    getProductById,
    onDelete,
    updateProduct,
    updateProductStatus,
    searchByName,
    addStockQty,
    status,
    statusDelete
  };
});
