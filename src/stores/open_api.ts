import { defineStore } from "pinia";
import axios from "axios";
import { ref } from "vue";

import.meta.env.VITE_APP_API_URL;

export const useOpenApiStore = defineStore("openApi", () => {
  const list_menu = ref();

  async function getMenu(category_id: any, page: any) {
    let data = JSON.stringify({
      "limit": 9,
      "page": page,
      "id": 0,
      "name": "string",
      "category_id": category_id
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `openapi/menu`,
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        list_menu.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }
  async function getMenuHome(category_id: any, page: any) {
    let data = JSON.stringify({
      "limit": 50,
      "page": page,
      "id": 0,
      "name": "string",
      "category_id": category_id
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `openapi/menu`,
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        list_menu.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }
  const list_chefs = ref();
  async function getListChef(){
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `openapi/list-chef`,
      headers: {
        'accept': '*/*'
      }
    };
    await axios.request(config)
        .then((response) => {
          list_chefs.value = response.data.data;
        })
        .catch((error) => {
          console.log(error);
        });

  }
  const listCategory = ref();

  async function getListCategory() {
    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `openapi/categories`,
      headers: {
        accept: "*/*",
      },
    };

    await axios
      .request(config)
      .then((response) => {
        listCategory.value = response.data.data;
        console.log(listCategory.value);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const listCart = ref();

  async function getListCart(code: any) {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `cart/list?code=`+code,
      headers: {
        accept: "*/*",
      },
    };
    await axios
      .request(config)
      .then((response) => {
        listCart.value = response.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async function addToCart(product_id: any, product_qty: any, note: any,code: any) {
    let data = JSON.stringify({
      id: 0,
      table_id: 0,
      product_id: product_id,
      qty: product_qty,
      note: note,
    });

    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `cart/add?code=`+code,
      headers: {
        accept: "*/*",
        "Content-Type": "application/json",
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async function updateCartItem(cartId: any, qty: any){
    let data = JSON.stringify({
      "id": cartId,
      "table_id": 0,
      "product_id": 0,
      "qty": qty,
      "note": ""
    });

    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `cart/update`,
      headers: {
        'accept': '*/*',
        'Content-Type': 'application/json'
      },
      data : data
    };

    await axios.request(config)
        .then((response) => {
          console.log(JSON.stringify(response.data));
        })
        .catch((error) => {
          console.log(error);
        });

  }

  async function deleteCart(id: any) {
    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `cart/delete/` + id,
      headers: {
        accept: "*/*",
      },
    };

    await axios
      .request(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const message = ref();

  async function placeOrder(code: any) {
    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `openapi/order?code=`+code,
      headers: {
        accept: "*/*",
      },
    };
    await axios
      .request(config)
      .then((response) => {
        message.value =response.data.message;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const listTable = ref();
  async function getListTable(){

    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `openapi/list-table`,
      headers: {
        'accept': '*/*'
      }
    };

    await axios.request(config)
      .then((response) => {
       listTable.value = response.data.data;
      })
      .catch((error) => {
        console.log(error);
      });

  }
  async function pushNotification(id: any, date: any, url: any){
    let data = JSON.stringify({
      "table_id": id,
      "date": date,
      "url": url
    });

    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `/api/notifications/send-notification`,
      headers: {
        'accept': '*/*',
        'Content-Type': 'application/json'
      },
      data : data
    };

    await axios.request(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
      })
      .catch((error) => {
        console.log(error);
      });

  }
  const checkAcceptOrder = ref(false);
  async function checkAccept(){
    let config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: `openapi/accept`,
      headers: {
        'accept': '*/*'
      }
    };

    await axios.request(config)
        .then((response) => {
         if (response.data.data == "Y"){
           checkAcceptOrder.value = true;
         }else{
           checkAcceptOrder.value = false;
         }
          console.log(checkAcceptOrder.value);
        })
        .catch((error) => {
          console.log(error);
        });

  }
  async function changeAccpet(status: any, reason: any, length: any){
    let data = JSON.stringify({
      "id": 0,
      "length_time": length,
      "reason": reason,
      "status": status
    });

    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `openapi/accept-order`,
      headers: {
        'accept': '*/*',
        'Content-Type': 'application/json'
      },
      data : data
    };

    await axios.request(config)
        .then((response) => {
          // console.log(JSON.stringify(response.data));
        })
        .catch((error) => {
          console.log(error);
        });

  }
  const content = ref<any>([]);
  async function getContent(){
    let config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: `openapi/contents`,
      headers: {
        'accept': '*/*'
      }
    };
    await axios.request(config)
        .then((response) => {
          content.value = response.data.data;
        })
        .catch((error) => {
          console.log(error);
        });
  }

  return {
    getMenu,
    list_menu,
    getListCategory,
    listCategory,
    listCart,
    getListCart,
    addToCart,
    deleteCart,
    placeOrder,
    listTable,
    getListTable,
    pushNotification,
    updateCartItem,
    message,
    checkAcceptOrder,
    checkAccept,
    changeAccpet,
    getMenuHome,
    list_chefs,
    getListChef,
    content,
    getContent
  };
});
