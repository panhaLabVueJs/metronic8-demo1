import { defineStore } from "pinia";
import JwtService from "@/core/services/JwtService";
import axios from "axios";
import { ref } from "vue";

import.meta.env.VITE_APP_API_URL;
export const useUploadStore = defineStore("upload", () => {
  const token = `Bearer ${JwtService.getToken()}`;
  const getImage = ref();

  async function doUploadFile(file: File) {
    let data = new FormData();
    data.append("File", file);
    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `app/public/v1/image/upload`,
      headers: {
        accept: "*/*",
        Authorization: token,
      },
      data: data,
    };

    await axios
      .request(config)
      .then((response) => {
        getImage.value = response.data.data.data;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  return {
    doUploadFile,
    getImage,
  };
});
